# ConfigMap & Volume


* 一个容器镜像适应不同的环境要求：开发，测试，生产
* 尽量复用镜像，保持统一，避免重复构建环境
* ConfigMap可以将key/value配置注入容器

## Create

```yml
apiVersion: v1
kind: ConfigMap
metadata:
  name: whoami-config
data:
  parameter1: value1
  parameter2: value2
```

kubectl apply -f busybox-confi.yml 


## Create from file

kubectl create cm traefik-conf --from-file=traefik.toml
kubectl create cm whoami-conf --from-file=application-prod.properties

## Show

* kubectl get cm busybox-config
* kubectl describe cm busybox-config


## Use

### use key/value

```yml
      containers:
      - name: busybox
        image: busybox
        command: ["/bin/sh", "-c", "env"]
        env:
          - name: ANOTHER_PARAM
            valueFrom:
              configMapKeyRef:
                name: whoami-config
                key: parameter1
        volumeMounts:
          - name: config-volume
            mountPath: /config     
        ports:
        - containerPort: 80
      volumes:
      - name: config-volume
        configMap:
          name: whoami-config
```

### use file
```yml
	args:
        - --configfile=/config/traefik.toml
        - --api
        - --web
        - --kubernetes
        - --logLevel=INFO
        volumeMounts:
        - mountPath: "/config"
          name: "config"
```

